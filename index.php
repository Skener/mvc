<?php

require 'vendor/autoload.php';


use Core\Router;

session_start();
include 'App/Config.php';

$url = ltrim( $_SERVER['REQUEST_URI'], '/' );

$router = new Router();

$router->add( '', [ 'controller' => 'HomeController', 'action' => 'index' ] );
$router->add( 'login', [ 'controller' => 'HomeController', 'action' => 'login' ] );
$router->add( 'signin', [ 'controller' => 'HomeController', 'action' => 'loginform' ] );
$router->add( 'posts', [ 'controller' => 'PostController', 'action' => 'index' ] );
$router->add( 'logout', [ 'controller' => 'HomeController', 'action' => 'logout' ] );
$router->add( '{controller}/{action}' );
$router->add( 'posts/{id:\d+}', [ 'controller' => 'PostController', 'action' => 'post' ] );


$router->dispatch( $url );


echo 'Routes<br>';
var_dump($router->routes);
echo 'Params<br>';
var_dump($router->params);




