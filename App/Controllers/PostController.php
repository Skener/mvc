<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;

class PostController extends Controller {

	protected $apiurl = 'https://jsonplaceholder.typicode.com/posts';


	public function index() {
		$session = $_SESSION['logedIn'];
		$data    = $this->apiQuery( $this->apiurl );
		$posts   = json_decode( $data, true );
		View::renderTemplate( 'Posts/index.html', [ 'posts' => $posts, 'session' => $session ] );

	}

	public function post() {
		$session = $_SESSION['logedIn'];
		$id      = $this->route_params['id'];
		$url     = $this->apiurl . '/' . $id;
		$data    = $this->apiQuery( $url );
		$post    = json_decode( $data, true );

		View::renderTemplate( 'Posts/post.html', [ 'post' => $post, 'session' => $session ] );
	}

	protected function apiQuery( $url ) {
		return file_get_contents( $url );
	}
}