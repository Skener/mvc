<?php


namespace App\Controllers;

use App\Models\User;
use Core\Controller;
use Core\View;


class HomeController extends Controller {

	public function index() {

		$_SESSION['logedIn'] = false;

		View::renderTemplate( 'Home/index.html', [ 'name' => 'Skener', 'colors' => [ 'red', 'green', 'blue' ] ] );
	}

	public function loginform() {
		View::renderTemplate( 'Home/login.html' );
	}


	public function login() {
		$user      = new User();
		$curr_user = [];
		$curr_user = $user->validate();
		if ( $curr_user === false ) {
			echo $user->errors;
			exit();
		}
		$db_user = $curr_user->findUserName( $curr_user->user['name'] );

		$role = $db_user['role'];
		if ( $db_user !== false ) {
			foreach ( $db_user as $key => $value ) {
				$curr_user->user[ $key ] = $value;
			}
			if ( $db_user['password'] !== $curr_user->user['password'] ) {
				echo "<h3>{$curr_user->user['name']}, sorry you are  not registered!</h3>";
			} elseif ( $curr_user->user['role'] == 'admin' ) {
				echo "Hello Admin: <h3>{$curr_user->user['name']}</h3>";
			} else {
				echo "Welcome dear <h4>{$curr_user->user['name']}</h4> ";
			}
			$_SESSION['user']    = (array) $curr_user;
			$_SESSION['logedIn'] = true;
			View::renderTemplate( 'Home/home.html', [ 'curr_user' => $curr_user ] );

		} else {
			$_SESSION['logedIn'] = false;
			echo "<h3>{$curr_user->user['name']}, sorry you are  not registered!</h3>";
		}
	}


	public function logout() {

		$_SESSION = array();

		if ( ini_get( "session.use_cookies" ) ) {
			$params = session_get_cookie_params();
			setcookie( session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}


		session_destroy();

		View::renderTemplate( 'Home/index.html' );

	}

}