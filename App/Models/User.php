<?php


namespace App\Models;


use Core\Model;
use PDO;
use Core\View;

class User extends Model {

	public $user = [];
	/**
	 * Error messages
	 *
	 * @var array
	 */
	public $errors = [];

	/**
	 * Class constructor
	 *
	 * @param array $data Initial property values
	 *
	 * @return void
	 */
	public function __construct( $data = [] ) {
		foreach ( $data as $key => $value ) {
			$this->user[ $key ] = $value;
		}
	}

	public function findUserName( $name = '' ) {
		try {
			$db                  = static::getSQLite();
			$res                 = $db->query( "select  name, password, role  from users where name LIKE '{$name}' and password ={$this->user['password']}" );
			$res                 = $res->fetch( PDO::FETCH_ASSOC );
			//$_SESSION['logedIn'] = true;
			session_regenerate_id( true );
		} catch ( PDOException $e ) {
			print ( "exception " . $e->getMessage() );
		}


		return $res;
	}


	public function validate() {

		if ( ( $_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['submit'] === 'Login' ) ) {

			if ( $_POST['name'] !== '' || $_POST['password'] !== '' ) {
				$name                   = filter_var( $_POST['name'], FILTER_SANITIZE_STRING );
				$password               = filter_var( $_POST['password'], FILTER_SANITIZE_STRING );
				$this->user['name']     = $name;
				$this->user['password'] = $password;
				$this->errors           = [];

				return $this;
			} else {
				$this->errors = 'Please fill required field';

				return false;
			}
		}

	}
}
