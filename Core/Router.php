<?php


namespace Core;


class Router {
	//Routes table(['route'=>['controller'=>'', 'action'=>'']]) declared from Add method.
	public $routes = [];

	//Current params ['controller'=>'', 'action'=>''].
	public $params = [];

	public function add( string $route, array $params =  [] ) {
		// Convert the route to a regular expression: escape forward slashes
		$route = preg_replace('/\//', '\\/', $route);
		// Convert variables e.g. {controller}
		$route = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)', $route);
		// Convert variables with custom regular expressions e.g. {id:\d+}
		$route = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $route);
		// Add start and end delimiters, and case insensitive flag
		$route = '/^'.$route.'$/i';
		$this->routes[$route] = $params;
	}


	public function match( $url ) {
		foreach ( $this->routes as $route => $params ) {
			if ( preg_match( $route, $url, $mathes ) ) {
				foreach ( $mathes as $key => $match ) {
					if ( is_string( $key ) ) {
						$params[ $key ] = $match;
					}
				}
				$this->params = $params;
				return true;
			}
		}

		return false;
	}


	public function dispatch( $url ) {

		if ( $this->match( $url ) ) {
			$controller =  'App\Controllers\\' . $this->params['controller'];
			if ( class_exists( $controller ) ) {
				$controller_object = new $controller( $this->params );

				$action = $this->params['action'];

				if ( is_callable( $action, true ) ) {
					$controller_object->$action();
				} else {
					echo 'Method  ' . $action . '  not found';
				}
			} else {
				echo 'Controller ' . $controller . ' not exist';
			}
		} else {
			echo "No route matched.";
		}

	}

}
