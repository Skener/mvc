<?php

namespace Core;

use PDO;
use App\Config;


class Model {
	/**
	 * Get the PDO database connection
	 *
	 * @return mixed
	 */
	protected static function getDB() {
		static $db = null;

		if ( $db === null ) {
			$dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';charset=utf8';
			$db  = new PDO( $dsn, Config::DB_USER, Config::DB_PASSWORD );

			// Throw an Exception when an error occurs
			$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		}

		return $db;
	}

	protected static function getSQLite() {
		static $db = null;
		if ( $db === null ) {
			$db = new PDO( "sqlite:db_users.sqlite" );
			$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			return $db;
		}
	}
}