<?php


namespace Core;
use Twig;

class View {

	public static function render( $view, $args = [] ) {
		extract( $args, EXTR_SKIP );
		$file = __DIR__."/{$view}";  // relative to Core directory
		if ( is_readable( $file ) ) {
			require $file;
		} else {
			throw new \Exception( "$file not found" );
		}
	}

	public static function renderTemplate( $template, $args = [] ) {

		static $twig = null;
		if ( $twig === null ) {
			$loader = new Twig\Loader\FilesystemLoader( 'App/Views' );
			$twig = new Twig\Environment($loader, [
				//'debug' => true,
				// ...
			]);
			$twig->addExtension(new \Twig\Extension\DebugExtension());
		}
		echo $twig->render( $template, $args );
	}

}